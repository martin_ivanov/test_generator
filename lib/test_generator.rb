require "./lib/problems_generator"
require "pdfkit"

def build_test difficulty 
	problems = Array.new
	
	if difficulty == "hard" 
		12.times do 
			problems << hard_question
		end
	elsif difficulty == "easy"
		12.times do 
			problems << simple_question
		end
	else
		return "You've entered bad arguments. Try again please"
	end

	problems
end

#creates test represented in html format and 
#returns their solutions for further usage
def build_html_and_pdf_representation i, problems
	results = Array.new
	myfile = File.new("tests/#{i.to_s}.html", "w+") 

	myfile.puts "<html>"
	myfile.puts "<title>Test #{i.to_s}</title>"
	myfile.puts "<style>body > div{clear:both;float:left; width:500px;}</style>"
	myfile.puts "<body style=\"padding: 15px;\">"
	
	problems.each_with_index do |p,index|
		results << p.split("\n").last
		p = p.split("\n")[0..-2]
		myfile.puts "<div style=\"border-bottom: 1px dotted black; margin:20px;\">"
		myfile.puts (index+1).to_s + "<br><br>"
		p.each do |e|
			myfile.puts e + "<br>"
		end
		myfile.puts "</div>"
	end 

	myfile.puts "</body>"
	myfile.puts "</html>"

	kit = PDFKit.new(myfile)
	kit.to_file("tests/#{i}.pdf")
	
	results
end
 
#
def build_answers_representation i, results
	myfile = File.new("tests/#{i.to_s}_results.html", "w+") 

	myfile.puts "<html>"
	myfile.puts "<title>Test #{i.to_s}</title>"
	myfile.puts "<style>body > div{clear:both;float:left; width:500px;}</style>"
	myfile.puts "<body style=\"padding: 15px;\">"

		results.each_with_index do |row,index|
			myfile.puts (index+1).to_s + " - " + row + "<br>"
		end
	
	myfile.puts "</body>"
	myfile.puts "</html>"

	kit = PDFKit.new(myfile)
	kit.to_file("tests/#{i}_results.pdf")
end	