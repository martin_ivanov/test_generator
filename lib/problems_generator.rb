# outputs random problem from the easy types
def get_simple_type hexes

	pool = [
	   "int a = #{hexes[0]} & 0xF00F;
		int b = #{hexes[1]} & 0x000F;
		int result = a #{["|", "&", "^"].sample} (b #{["<<",">>"].sample} #{["4","8"].sample});",
		
	   "int a = #{hexes[0]};
		int b = #{hexes[1]};
		a = a #{["<<",">>"].sample} #{["3","5","7","9"].sample};
		b = b #{["<<",">>"].sample} #{["3","5","7","9"].sample};
		int result = a #{["|", "&", "^"].sample} b;",

	   "long value = #{hexes[0] + hexes[1].gsub("0x","")};
		int result=0;
		if (value #{["|", "&", "^"].sample} (1 << #{["4","8"].sample})){
			result = 1;
		}else{
			result = 2;
		}"
	]

	pool.sample.gsub("\t\t","")
end

# outputs random problem from the hard types
def get_hard_type hexes 
	pool = [

	   "long value1 = #{hexes[0] + hexes[1].gsub("0x","")};
		long value2 = #{hexes[2] + hexes[3].gsub("0x","")};
		int result = (value1 #{["<<",">>"].sample} #{["3","5","7","9"].sample}) #{["|", "&", "^"].sample} (value2 #{["<<",">>"].sample} #{["3","5","7","9"].sample});",

	   "long testValue = #{hexes[0] + hexes[1].gsub("0x","")};
		int a =0;
		int result = 0;
		if ( (result = testValue #{["|", "&", "^"].sample} testValue #{["|", "&", "^"].sample} testValue #{["|", "&", "^"].sample} (1 #{["<<",">>"].sample} #{["4","8"].sample})) ){
			a = 1;
		}else{
			a = 2;
		}
		printf(\"a = \%d, \", a);",
		
	   "int value1 = #{rand(100...500)};
		int value2 = #{rand(100...500)};
		int result = (value1 #{["<<",">>"].sample} #{["3","5","7","9"].sample}) #{["|", "&", "^"].sample} (value2 #{["<<",">>"].sample} #{["3","5","7","9"].sample});"	
	
	]

	pool.sample.gsub("\t\t","")
end

#gets easy problem and solves it 
def simple_question 
	hexes = random_hex_numbers 2
	problem = get_simple_type hexes
	File.open("try.c", 'w') do |file| 
		file.write(
		"#include <stdio.h>
		int main(){
			
			#{problem}

			printf(\"result = \%d\", result);
			
			return 0;
		}") 
	end

	`gcc try.c`
	result = `./a.out`
	`rm try.c a.out`

	problem + "\n" + result
end

#gets hard problem and solves it 
def hard_question
	hexes = random_hex_numbers 4
	problem = get_hard_type hexes
	File.open("try.c", 'w') do |file| 
		file.write(
		"#include <stdio.h>
		int main(){
			
			#{problem}

			printf(\"result = \%d\", result);
			
			return 0;
		}") 
	end

	`gcc try.c`
	result = `./a.out`
	`rm try.c a.out`

	return problem + "\n" + result
end

#generates random 32-bit hex number
def random_hex_numbers n
	pool = ["0","1","2",
			"3","4","5",
			"6","7","8",
			"9","A","B",
			"C","D","E",
			"F"]

	output = Array.new
	(1..n).each do |num|
		num = "0x#{pool.sample}#{pool.sample}#{pool.sample}#{pool.sample}"
		output << num
	end

	output
end