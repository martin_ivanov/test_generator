require "./lib/test_generator"

if ARGV[0].nil? 
	puts "You havent entered any arguments"
else
	requestedTests = ARGV[0].to_i
	puts "requestedTests = " + requestedTests.to_s
	`mkdir tests`

	i=1
	requestedTests.times do 
		problems = build_test ARGV[1].downcase
		if problems.class == String
			puts problems
			return
		else
			results = build_html_and_pdf_representation i, problems
			build_answers_representation i, results
			i+=1
		end
	end
end