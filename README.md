test_generator
==============

Generates tests with bitwise operators

==============

Dependencies:
	1 - ruby 1.9.3p484
	2 - pdfkit
	3 - wkhtmltopdf

==============

Usage:
	Run from main.rb 
	argv 0 - the quantity of test you want
	argv 1 - the mode you want - hard or easy

	sample input:
		ruby main.rb 10 hard
		(generates 10 tests with 12 hard problems each)
	
